package vn.com.lending.userservice.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import vn.com.lending.userservice.exeption.UnprocessableEntityException;
import vn.com.lending.userservice.model.Codes;
import vn.com.lending.userservice.model.User;
import vn.com.lending.userservice.repository.OTPRepository;
import vn.com.lending.userservice.repository.UserRepository;
import vn.com.lending.userservice.response.ErrorRs;
import vn.com.lending.userservice.service.UserService;
import vn.com.lending.userservice.web.LoginRq;
import vn.com.lending.userservice.web.RegisterRq;
import vn.com.lending.userservice.web.Response;

@Slf4j
@RestController
public class UserController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;
//	@Autowired
//	private OTPRepository otpRepository;
	
	@PostMapping(value = "/register")
	@ResponseBody
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found Exception", response = ErrorRs.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorRs.class) })
	public Response register(@RequestBody RegisterRq registerRq, @RequestHeader String requestId) {
		log.info("Register request_id [{}],body [{}]",requestId,registerRq);
		List<User> ls = userRepository.findByUsername(registerRq.getUser());
		if(ls.size() > 0) {
			throw new UnprocessableEntityException("user_name are exits");
		}
//		Codes code = otpRepository.findByHash(registerRq.getToken());
//		if (code != null && code.getPhoneNumber().compareTo(registerRq.getUser()) != 0) {			
//			throw new UnprocessableEntityException("Token not map or phone not map");
//		}
    	UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		User user = new User();
		user.setUserId(randomUUIDString);
		user.setUsername(registerRq.getUser());
		user.setPassword(registerRq.getPassword());
		user.setDate(LocalDateTime.now());
		userRepository.save(user);
		return new Response("00","Success",user.getUserId());
	}

	@PostMapping(value = "/login")

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 404, message = "Not Found Exception", response = ErrorRs.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorRs.class) })
	public Response login(@RequestBody LoginRq registerRq, @RequestHeader String requestId) {
		log.info("Login request_id [{}],body [{}]",requestId,registerRq);
		if(registerRq.getUser().isEmpty() || registerRq.getPassword().isEmpty()) {
			throw new UnprocessableEntityException("user_name or password is null");
		}
		List<User> ls = userRepository.findByUsername(registerRq.getUser());
		if(ls.size() < 1) {
			throw new UnprocessableEntityException("user_name are not exits");
		}
		User user = userService.getUser(registerRq.getUser(), registerRq.getPassword());
		if (user.getIdRec() != null) {
			return new Response("00","Success",user.getUserId());
		}		
		throw new UnprocessableEntityException("user_name or password not correct");
	}

}
