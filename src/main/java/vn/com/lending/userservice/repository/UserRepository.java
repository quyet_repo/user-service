package vn.com.lending.userservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.lending.userservice.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	List<User> findByUsername(String username);
	
}
