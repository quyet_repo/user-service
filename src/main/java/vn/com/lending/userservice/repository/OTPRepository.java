package vn.com.lending.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.lending.userservice.model.Codes;

public interface OTPRepository extends JpaRepository<Codes, Integer> {

    Codes findByRequestId(String requestId);
    Codes findByHash(String hash);
    
}
