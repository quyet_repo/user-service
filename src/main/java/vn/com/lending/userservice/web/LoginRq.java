package vn.com.lending.userservice.web;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRq {
	@NotNull
	private String user;
	@NotNull
	private String password;
}
