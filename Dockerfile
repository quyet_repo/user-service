FROM java:openjdk-8u91-jdk
CMD java ${JAVA_OPTS} -jar *.jar
EXPOSE 8081
COPY build/libs/*.jar .